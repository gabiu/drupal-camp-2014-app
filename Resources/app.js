//
//  app.js
//  DrupalCamp
//
//  Created by AG Prime on 2014-05-15.
//  Copyright 2014 AG Prime. All rights reserved.
//

var moment = require('/lib/moment');
var global_settings = require('/settings/global');
var uiSettings = require('/settings/uiSettings');
var dal = require('/lib/db/dal');

dal.init('drupalcampdb');

// Mvc is our Model-View-Controller object
var Mvc = require('/mvc/mvc');

// Load the definitions of the controllers
require('/mvc/controllers/maincontroller');

function is7() {

	if (Ti.Platform.osname == 'iphone') {
		if (Ti.Platform.version.substr(0, 1) == '7') {
			return true;
		}
		if (Ti.Platform.version.substr(0, 1) == '8') {
			return true;
		}
		return false;
	}
	else {
		return false;
	}
}

var actInd = function(top) {

	var style;

	if (Ti.Platform.name == 'android') {

		style = Ti.UI.ActivityIndicatorStyle.DARK;
	}
	else {

		style = Ti.UI.iPhone.ActivityIndicatorStyle.DARK;
	}

	var activityIndicator = Ti.UI.createActivityIndicator({
		message : 'Loading...',
		style : style,
		top : top ? top : '50%',
		height : Ti.UI.SIZE,
		width : Ti.UI.SIZE
	});

	return activityIndicator;
};

var android = Ti.Platform.name == 'android' ? true : false;

Mvc.init();
// Map a default route
Mvc.mapRoute('Default_Route', '{controller}.{action}.{text}', // could also be
// {controller}/{action}/{id}
{
	controller : 'Home',
	action : 'Default',
	text : 'Default Text'
});

// manage the windows -- window stack
var openViews = [];

// Contents of the view are in parameter ui. Create a new window if desired. Or
// create a view
// on top of the current window. Depends on routeData.

var navGroup,
    iosWin,
    mainWindow,
    leftBtn,
    winLeft,
    winRightView;

Mvc.render = function(ui, routeData) {

	if (ui.preventRender === true) {

		return ui;
	}

	var win,
	    index,
	    routeName = routeData.controller + "." + routeData.action;

	// Storing the routeData in the ui view, for better management
	ui.routeName = routeName;

	if (!mainWindow) {

		mainWindow = Ti.UI.createWindow({
			navBarHidden : true,
			fullscreen : false,
			exitOnClose : true
		});

		mainWindow.navBar = require('/ui/common/navBar')();
		mainWindow.add(mainWindow.navBar);

		mainWindow.contentArea = Ti.UI.createView(uiSettings.contentArea);
		mainWindow.contentArea.add(ui);
		openViews.push(ui);
		mainWindow.add(mainWindow.contentArea);

		mainWindow.tabMenu = require('/ui/common/tabMenu')();
		mainWindow.add(mainWindow.tabMenu);

		mainWindow.open();
	}
	else {

		mainWindow.contentArea.add(ui);

		// Closing everything else than this newly added view
		for (var i = 0; i < mainWindow.contentArea.children.length - 1; i++) {

			Ti.API.info('length ' + openViews.length + ' ' + mainWindow.contentArea.children.length);

			if (mainWindow.contentArea.children[i]) {

				mainWindow.contentArea.remove(mainWindow.contentArea.children[i]);
				openViews.splice(i, 1);
			}
		}

		openViews.push(ui);
	}

};

Mvc.start('Main.Schedule');

