//
//  dal.js
//  DrupalCamp
//
//  Created by AG Prime on 2014-05-21.
//  Copyright 2014 AG Prime. All rights reserved.
//

var drupal_api = require('/lib/drupal_api'); 

var dal = {};
var models = {};
var joli;

dal.ScheduleModels = require("/lib/db/data_models/schedule_models");

// Init function receives name of database to be opened as parameter
dal.init = function(database) {

	joli = require("/lib/db/joli").connect(database);

	// Run setup for all models we'll use in the application
	this.ScheduleModels.Initialize(models, joli);

	// Models have been setup. Initialize the database
	joli.models.initialize();
};

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Transaction
//

dal.beginTransaction = function() {

	dal.trans = new joli.transaction();

	dal.trans.begin();
};

dal.commitTransaction = function() {

	if (dal.trans != undefined) {

		dal.trans.commit();
	}
};

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Schedules
//

dal.insertSchedule = function(rawValues) {
	
	// schedule_details sanitization. Removes the multiple spaces and newlines from the start and end of the string.
	function sanitize(text) {
		
		text = text.trim();
		
		var regex = /^(\\r|\\n|\\t)|(\\r|\\n|\\t)$/g;
		
		if (text.match(regex)) {
			
			text = text.replace(regex, '');
		}
		return text;
	}
	
	var imageURL = rawValues.speaker_picture.split('src=\"');
	imageURL = imageURL[1].split('\"')[0];
	
	// mapping the values to the model and adding them to the local db
	var mappedValues = {
		speaker_uid : rawValues.speaker_uid,
		schedule_date : moment(rawValues.schedule_date).toJSON(),
		schedule_title : rawValues.schedule_title,
		speaker_name : rawValues.speaker_name,
		company : rawValues.company,
		schedule_time : rawValues.schedule_time,
		schedule_type : rawValues.schedule_type,
		speaker_picture : imageURL,
		schedule_details : sanitize(rawValues.schedule_details.replace(/&(nbsp|amp|quot|lt|gt);/g, ' '))
	}

	var newSchedule = models.schedules.newRecord(mappedValues);

	newSchedule.save();
};

// Delete all the schedules from the database.
dal.deleteSchedules = function() {

	models.schedules.deleteRecords();
};

dal.downloadSchedule = function(success, fail) {
	
	function onSuccess(data) {
		
		Ti.App.Properties.setInt('lastUpdate', new Date().getTime() / 1000);
		
		dal.deleteSchedules();
		
		for (var element in data) {
			
			dal.insertSchedule(data[element]);
		}
		
		if (success) {
			
			success();
		}
	}
	
	function onFail(error) {
		
		alert(error);
		
		if (fail) {
			
			fail();
		}
	}
	
	drupal_api.get_schedule(onSuccess, onFail);
}

dal.getSchedules = function() {
	
	Ti.API.info('have ' + models.schedules.count());
	return models.schedules.all();
}; 

dal.getSpeakers = function() {
	
	var schedules = models.schedules.all();

	var speakersArray = {};
	
	for (var i in schedules) {

		if (schedules[i].speaker_name != '-') {
			
			var speaker_name = schedules[i].speaker_name;
			
			if (!speakersArray[speaker_name]) {
				
				speakersArray[speaker_name] = [];
			}
			
			speakersArray[speaker_name].push(schedules[i]);
		}
	}

	return speakersArray;
}; 


// Make the dal object available to the outside world
module.exports = dal;
