//
//  contact_data_models.js
//  DrupalCamp
//
//  Created by AG Prime on 2014-05-21.
//  Copyright 2014 AG Prime. All rights reserved.
//

// The data model dealing with schedule entries
var scheduleModel = {};

scheduleModel.scheduleModelFields = {
	id : 'INTEGER PRIMARY KEY AUTOINCREMENT',
	speaker_uid : 'INTEGER',
	schedule_date : 'TEXT',
	schedule_title : 'TEXT',
	speaker_name : 'TEXT',
	company : 'TEXT',
	schedule_time : 'TEXT',
	schedule_type : 'TEXT',
	speaker_picture : 'TEXT',
	schedule_details : 'TEXT'
};

// Initialization extends the models object passed as a parameter in this
// function.
// It uses the joli instance passed as second parameter.
scheduleModel.Initialize = function(models, joli) {

	// scheduleModel
	// This model contains the actual contact data
	models.schedules = new joli.model({
		table : 'Schedules',
		columns : this.scheduleModelFields
	});
};

module.exports = scheduleModel;
