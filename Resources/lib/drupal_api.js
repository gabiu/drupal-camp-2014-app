// 
//  drupal_api.js
//  DrupalCamp
//  
//  Created by AG Prime on 2014-05-21.
//  Copyright 2014 AG Prime. All rights reserved.
// 

var config = require('/settings/config');

var drupal_api = {};

function beautify_error(error) {

	Ti.API.info("beautify " + JSON.stringify(error));

	if (error.message) {

		return error.message;
	}

	return error;
};

/*
 * General function that will get all data.
 * @param
 * 	url - the url that will be called
 * 	method - the used method
 * 	successFunction - the function called on success
 * 	errorFunction - the function called on error
 */
function httpRequest(url, method, data, successFunction, errorFunction) {
	
	var xhr = Ti.Network.createHTTPClient();

	xhr.onload = function() {

		if (this.status == '200') {

			try {

				Ti.API.info(url, this.responseText, 'sent', JSON.stringify(data));
				var responseJSON = JSON.parse(this.responseText);

				if (responseJSON.success != undefined || responseJSON != null) {

					if (responseJSON.success == false) {

						errorFunction(beautify_error(responseJSON));
						return;
					}
				}

				successFunction(responseJSON);
			}
			catch (e) {

				errorFunction(beautify_error(e));
				Ti.API.error(e);
			}
		}
		else {

			errorFunction(beautify_error(this.responseText));
			Ti.API.error(this.responseText);
		}
	};

	xhr.onerror = function(e) {

		Ti.API.info('Transmission error: ' + url + ' ' + JSON.stringify(this), 'sent', JSON.stringify(data));

		if (this.responseText) {
			
			errorFunction(beautify_error(this.responseText));
		}
		else {

			errorFunction(beautify_error(e));
		}
	};
	
	xhr.timeout = 20000;

	xhr.open(method, url);

	xhr.setRequestHeader('Authorization', 'Basic ' + Ti.Utils.base64encode('dev:dev'));

	xhr.setRequestHeader("Content-Type", "application/json");

	if (data != undefined || data != null) {

		xhr.send(data);
	}
	else {

		xhr.send();
	}
};

drupal_api.get_schedule = function(success, fail) {

	httpRequest(config.scheduleUrl, 'GET', null, success, fail);
};

drupal_api.update_required = function(success, fail) {
	
	httpRequest(config.updateRequiredUrl, 'POST', {timestamp : Ti.App.Properties.getInt('lastUpdate', 0)}, success, fail);
};

module.exports = drupal_api;