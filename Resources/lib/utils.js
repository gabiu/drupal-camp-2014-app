// 
//  utils.js
//  DrupalCamp
//  
//  Created by AG Prime on 2014-05-21.
//  Copyright 2014 AG Prime. All rights reserved.
// 

var utils = {};

/*
 * Finds a specific view with the given route in the openViews array
 */
utils.findView = function(route) {
	
	for (var element in openViews) {
		
		if (openViews[element].routeName == route) {
			
			return openViews[element];
		}
	}
	
	return false;
}

module.exports = utils;