//
//  maincontroller.js
//  DrupalCamp
//
//  Created by AG Prime on 2014-05-16.
//  Copyright 2014 AG Prime. All rights reserved.
//

var strings = require('/settings/strings');
var uiSettings = require('/settings/uiSettings');
var dal = require('/lib/db/dal');
var utils = require('/lib/utils');
var drupal_api = require('/lib/drupal_api');

var Mvc = require('/mvc/mvc');

Mvc.Views.Main = {};

// Before we add the controller, we define the views it supports
require('/mvc/views/mainviews/info');
require('/mvc/views/mainviews/schedule');
require('/mvc/views/mainviews/speaker_details');
require('/mvc/views/mainviews/speakers');
require('/mvc/views/mainviews/venue');

Mvc.Controllers.MainController = {

	Info : function() {

		global_settings.CURRENTOPTION = 'Main.Info';
		global_settings.CURRENTTITLE = strings.views.info.title;
		global_settings.CURRENTNAVCOLOR = uiSettings.views.info.mainColor
		Ti.App.fireEvent('app:tab_changed');

		return this.view('Info');
	},
	Schedule : function() {

		global_settings.CURRENTOPTION = 'Main.Schedule';
		global_settings.CURRENTTITLE = strings.views.schedule.title;
		global_settings.CURRENTNAVCOLOR = uiSettings.views.schedule.mainColor
		Ti.App.fireEvent('app:tab_changed');

		var scheduleData = dal.getSchedules();

		if (Ti.Network.online) {

			drupal_api.update_required(function(data) {
				Ti.API.info('verifying update');

				if (data.update_required || scheduleData.length == 0) {

					Ti.API.info(' update required');
					dal.downloadSchedule(function() {

						var scheduleView = utils.findView('Main.Schedule');

						if (scheduleView) {

							var scheduleData = dal.getSchedules();
							scheduleView.update(scheduleData);
						}
					});
				}
			}, function(error) {

				alert(error);
			});
		}

		return this.view('Schedule', scheduleData);

	},
	Speaker_Details : function() {

		global_settings.CURRENTOPTION = 'Main.Speaker_Details';
		// global_settings.CURRENTTITLE = strings.views.info.title;
		global_settings.CURRENTNAVCOLOR = uiSettings.views.speakers.mainColor
		Ti.App.fireEvent('app:tab_changed');

		return this.view('Speaker_Details');
	},
	Speakers : function() {

		global_settings.CURRENTOPTION = 'Main.Speakers';
		global_settings.CURRENTTITLE = strings.views.speakers.title;
		global_settings.CURRENTNAVCOLOR = uiSettings.views.speakers.mainColor
		Ti.App.fireEvent('app:tab_changed');

		var speakersData = dal.getSpeakers();

		return this.view('Speakers', speakersData);
	},
	Venue : function() {

		global_settings.CURRENTOPTION = 'Main.Venue';
		global_settings.CURRENTTITLE = strings.views.venue.title;
		global_settings.CURRENTNAVCOLOR = uiSettings.views.venue.mainColor
		Ti.App.fireEvent('app:tab_changed');

		return this.view('Venue');
	},
};
