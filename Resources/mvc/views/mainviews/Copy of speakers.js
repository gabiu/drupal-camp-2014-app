/*

//
//  speakers.js
//  DrupalCamp
//
//  Created by AG Prime on 2014-05-16.
//  Copyright 2014 AG Prime. All rights reserved.
//

var uiSettings = require('/settings/uiSettings');
var strings = require('/settings/strings');

// Create the 'Main' object if it doesn't already exist
if (!Mvc.Views.Main) {

	Mvc.Views.Main = {};
}

// The login view
Mvc.Views.Main.Speakers = function(speakersData) {

	var self = this;

	var view = Ti.UI.createScrollView(uiSettings.viewsStyle);

	Ti.API.info(JSON.stringify(speakersData));

	for (var i in speakersData) {

		var speakerItem = Ti.UI.createView(uiSettings.speakerItem);

		var speakerImage = Ti.UI.createImageView(uiSettings.speakerImage);

		var imageURL = speakersData[i][0].speaker_picture;

		if (imageURL.indexOf('default-user') != -1) {

			imageURL = '/images/default-user.png';
		}

		speakerImage.image = imageURL;
		Ti.API.info(speakersData[i][0].speaker_picture);
		speakerItem.add(speakerImage);

		var infoWrapper = Ti.UI.createView({
			left : '95dp',
			top : 0,
			right : 0,
			layout : 'vertical',
			height : uiSettings.speakerImage.height
		});
		speakerItem.add(infoWrapper);

		var speakerName = Ti.UI.createLabel(uiSettings.speakerName);
		speakerName.text = speakersData[i][0].speaker_name;
		infoWrapper.add(speakerName);

		var speakerCompany = Ti.UI.createLabel(uiSettings.speakerCompany);
		speakerCompany.text = speakersData[i][0].company;
		infoWrapper.add(speakerCompany);

		var speakerSeeMore = Ti.UI.createView(uiSettings.speakerSeeMore);
		speakerItem.seeMoreLabel = Ti.UI.createLabel({
			text : strings.views.speakers.see_all_talks,
			color : '#fff',
			font : {
				fontFamily : 'Segoe-UI-Light',
				fontSize : '14dp',
			},
			left : '5dp',
			shown : true
		});

		speakerItem.cancelLabel = Ti.UI.createLabel({
			text : strings.views.speakers.cancel,
			color : '#fff',
			font : {
				fontFamily : 'Segoe-UI-Light',
				fontSize : '14dp',
			},
			left : '5dp',
			opacity : 0
		});

		speakerSeeMore.addEventListener('click', function(e) {

			function findSpeakerItemParent(source) {

				if (source.speakerItem === true) {
					return source;
				}
				else {
					return findSpeakerItemParent(source.parent);
				}
			}

			function toggleTalks(show) {

				Ti.API.info('e: ' + JSON.stringify(e));
				Ti.API.info('findSpeakerItemParent(e.source).talksView: ' + JSON.stringify(findSpeakerItemParent(e.source).talksView));


				if (show) {
					Ti.API.info('show');
					// findSpeakerItemParent(e.source).talksView.animate({
						// height : findSpeakerItemParent(e.source).talksView.calculatedHeight.toString() + 'dp',
						// duration : 300
					// });
					
					// v---- Added by mide765 ----v
					findSpeakerItemParent(e.source).talksView.height = '0dp';
					findSpeakerItemParent(e.source).talksView.calculatedHeight = '0dp';
					
					
					
					// findSpeakerItemParent(e.source).talksView.talksContainer.height = '0dp';
					// talksContainer.height = '0dp';
					// speakerItem.talksView.height = '0dp';
					
				}
				else {
					Ti.API.info('hide');
					// findSpeakerItemParent(e.source).talksView.animate({
						// height : 0,
						// duration : 300
					// });
					
					
					// v---- Added by mide765 ----v
					findSpeakerItemParent(e.source).talksView.height = '160dp';
					findSpeakerItemParent(e.source).talksView.calculatedHeight = '160dp';
					
					
					
					// findSpeakerItemParent(e.source).talksView.talksContainer.height = '160dp';
					// talksContainer.height = '160dp';
					// speakerItem.talksView.height = '160dp';
					
				}
			}

			toggleTalks(findSpeakerItemParent(e.source).seeMoreLabel.shown);

			Ti.API.info('click');

			if (!findSpeakerItemParent(e.source).seeMoreLabel.shown) {

				findSpeakerItemParent(e.source).seeMoreLabel.animate({
					opacity : 1,
					duration : 300
				});
				findSpeakerItemParent(e.source).cancelLabel.animate({
					opacity : 0,
					duration : 300
				});
				findSpeakerItemParent(e.source).seeMoreLabel.shown = true;
			}
			else {

				findSpeakerItemParent(e.source).seeMoreLabel.animate({
					opacity : 0,
					duration : 300
				});
				findSpeakerItemParent(e.source).cancelLabel.animate({
					opacity : 1,
					duration : 300
				});
				findSpeakerItemParent(e.source).seeMoreLabel.shown = false;
			}

		});

		
		
		//
		//	mide765 version
		//
		speakerSeeMore.addEventListener('click', function(e) {



		speakerSeeMore.add(speakerItem.seeMoreLabel);
		speakerSeeMore.add(speakerItem.cancelLabel);
		speakerItem.add(speakerSeeMore);

		speakerItem.talksView = Ti.UI.createView({
			backgroundColor : uiSettings.speakerSeeMore.backgroundColor,
			width : Ti.UI.FILL,
			height : 0,
			top : '122dp'
		});

		speakerItem.add(speakerItem.talksView);

		var talksContainer = Ti.UI.createView({
			top : '10dp',
			height : Ti.UI.SIZE,
			// height : '80dp',
			width : Ti.UI.FILL,
			backgroundColor : 'white',
			layout : 'vertical'
		});

		speakerItem.talksView.add(talksContainer);

		var paddingBottom = Ti.UI.createView({
			width : Ti.UI.FILL,
			height : '7dp',
			top : 0
		});

		// calculating the view animation height manually. Ti.UI.SIZE does not work
		speakerItem.talksView.calculatedHeight = 14;
		// bottom padding

		for (var talk in speakersData[i]) {

			speakerItem.talksView.calculatedHeight += 7 + 50;
			//top padding and the view height

			Ti.API.info(JSON.stringify(speakersData[i][talk]));

			var talkContainer = Ti.UI.createView({
				width : '90%',
				height : Ti.UI.SIZE,
				top : '7dp',
				layout : 'horizontal',
				backgroundColor : 'red'
			});

			var image = Ti.UI.createImageView({
				width : '20dp',
				left : 0,
				center : {
					y : '50%'
				},
				image : uiSettings.scheduleCategoryImages[speakersData[i][talk].schedule_type]
			});

			talkContainer.add(image);

			var titleContainer = Ti.UI.createView({
				width : Ti.UI.FILL,
				backgroundColor : 'blue',
				height : Ti.UI.SIZE,
				left : '5dp',
				top : 0
			});

			// titleContainer.addEventListener('postlayout', function f(e) {
			//
			// // titleContainer.removeEventListener('postlayout', f);
			// Ti.API.info('setting to ' + e.source.rect.height);
			// e.source.parent.height = e.source.rect.height;
			// });

			talkContainer.add(titleContainer);

			var titleInner = Ti.UI.createView({
				width : '100%',
				height : Ti.UI.SIZE,
				top : 0,
				layout : 'vertical',
				backgroundColor : 'green'
			});

			var title = Ti.UI.createLabel({
				width : Ti.UI.FILL,
				text : speakersData[i][talk].schedule_title,
				center : {
					y : '50%'
				},
				left : 0,
				top : 0,
				font : {
					fontFamily : 'Segoe-UI-Light',
					fontSize : '14dp',
				},
				height : Ti.UI.SIZE,
				color : '#616265',
				backgroundColor : 'yellow'
			});

			var time = Ti.UI.createLabel({
				color : '#aeafb0',
				width : Ti.UI.FILL,
				left : 0,
				top : 0,
				height : Ti.UI.SIZE,
				center : {
					y : '50%'
				},
				font : {
					fontFamily : 'Segoe-UI-Light',
					fontSize : '14dp',
				},
				text : ' (' + moment(speakersData[i][talk].schedule_date).calendar() + ')'
			});

			if (speakersData[i][talk].schedule_title + '  (' + moment(speakersData[i][talk].schedule_date).calendar() + ')'.length > 40) {

				speakerItem.talksView.calculatedHeight += 50;
			}

			titleInner.add(title);
			titleInner.add(time);

			titleContainer.add(titleInner);

			var titleUnderline = Ti.UI.createView({
				bottom : 0,
				height : '1dp',
				width : Ti.UI.FILL,
				backgroundColor : 'black',
				backgroundColor : uiSettings.speakerSeeMore.backgroundColor
			});

			titleContainer.add(titleUnderline);

			talksContainer.add(talkContainer);
		}

		talksContainer.add(paddingBottom);

		view.add(speakerItem);
	}

	view.addEventListener('postlayout', function() {

		Ti.App.fireEvent('app:view_finished_loading');
	});

	// Bottom view padding
	var bottomPadding = Ti.UI.createView({
		width : '100%',
		height : speakerItem.top
	});

	view.add(bottomPadding);

	return view;
};
*/