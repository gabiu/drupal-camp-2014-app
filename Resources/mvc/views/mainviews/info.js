//
//  info.js
//  DrupalCamp
//
//  Created by AG Prime on 2014-05-16.
//  Copyright 2014 AG Prime. All rights reserved.
//

var uiSettings = require('/settings/uiSettings');

// Create the 'Main' object if it doesn't already exist
if (!Mvc.Views.Main) {

	Mvc.Views.Main = {};
}

// The login view
Mvc.Views.Main.Info = function() {

	var self = this;

	var view = Ti.UI.createScrollView(uiSettings.viewsStyle);

	var logo = Ti.UI.createImageView({
		top : '20dp',
		image : 'http://cluj2015.drupalcamp.ro/sites/default/files/drupalcampclujnapoca2015.png',
		height : '120dp',
		width : 'auto'
	});

	var titleLabel = Ti.UI.createLabel({
		top : '20dp',
		text : 'Drupal Camp',
		font : {
			fontFamily : 'Segoe-UI-Light',
			fontSize : '22dp',
		}
	});

	var titleTwoLabel = Ti.UI.createLabel({
		top : '10dp',
		text : 'Cluj-Napoca 2015',
		font : {
			fontFamily : 'Segoe-UI-Light',
			fontSize : '22dp',
		}
	});

	var descriptionLabel = Ti.UI.createLabel({
		top : '20dp',
		width : '90%',
		height : Ti.UI.SIZE,
		textAlign : Ti.UI.TEXT_ALIGNMENT_CENTER,
		backgroundColor : '#d2d3d5',
		text : 'This event wants to bring together people from the IT industry who work with or want to find out more about the Drupal platform. The overall goals of Drupal Camp are to connect local and regional communities and to allow the sharing of knowledge.',
		font : {
			fontFamily : 'Segoe-UI-Light',
			fontSize : '15dp',
		}
	});
	
	var sponsorLabel = Ti.UI.createLabel({
		top : '40dp',
		text : 'Sponsorship',
		font : {
			fontFamily : 'Segoe-UI-Light',
			fontSize : '22dp',
		}
	});
	
	var sponsorOneLabel = Ti.UI.createLabel({
		top : '20dp',
		text : 'Diamond Sponsor',
		font : {
			fontFamily : 'Segoe-UI-Light',
			fontSize : '15dp',
		}
	});
	
	var sponsorOneImage = Ti.UI.createImageView({
		top : '5dp',
		image : 'http://cluj2015.drupalcamp.ro/sites/default/files/Organization/pitechplus.png',
		width : '90%',
		height : 'auto'
	});
	
	var sponsorTwoLabel = Ti.UI.createLabel({
		top : '20dp',
		text : 'Gold Sponsors',
		font : {
			fontFamily : 'Segoe-UI-Light',
			fontSize : '15dp',
		}
	});
	
	var sponsorTwoView = Ti.UI.createView({
		width : '90%',
		layout : 'horizontal',
		height : Ti.UI.SIZE
	});
	
	var sponsorTwoImageOne = Ti.UI.createImageView({
		top : '5dp',
		image : 'http://cluj2015.drupalcamp.ro/sites/default/files/styles/large/public/Organization/agprimelogonewmoto.png',
		width : '45%',
		height : 'auto'
	});
	
	var sponsorTwoImageTwo = Ti.UI.createImageView({
		top : '5dp',
		left : '5%',
		image : 'http://cluj2015.drupalcamp.ro/sites/default/files/styles/large/public/Organization/print-cmyk-whitebg%281%29.png',
		width : '45%',
		height : 'auto'
	});
	
	
	
	
	var sponsorThreeLabel = Ti.UI.createLabel({
		top : '20dp',
		text : 'Silver Sponsors',
		font : {
			fontFamily : 'Segoe-UI-Light',
			fontSize : '15dp',
		}
	});
	
	var sponsorThreeView = Ti.UI.createView({
		width : '90%',
		layout : 'horizontal',
		height : Ti.UI.SIZE
	});
	
	var sponsorThreeImageOne = Ti.UI.createImageView({
		top : '5dp',
		image : 'http://cluj2015.drupalcamp.ro/sites/default/files/styles/medium/public/Organization/amplexor.png',
		width : '32%',
		height : 'auto'
	});
	
	var sponsorThreeImageTwo = Ti.UI.createImageView({
		top : '5dp',
		left : '15%',
		image : 'http://cluj2015.drupalcamp.ro/sites/default/files/styles/medium/public/Organization/reea_logo_big.png',
		width : '16%',
		height : 'auto'
	});
	
	var sponsorThreeImageThree = Ti.UI.createImageView({
		top : '5dp',
		left : '15%',
		image : 'http://cluj2015.drupalcamp.ro/sites/default/files/styles/medium/public/Organization/logo_intellix.png',
		width : '16%',
		height : 'auto'
	});
	
	
	var followLabel = Ti.UI.createLabel({
		top : '40dp',
		text : 'Follow Us',
		font : {
			fontFamily : 'Segoe-UI-Light',
			fontSize : '22dp',
		}
	});
	
	var followView = Ti.UI.createView({
		width : '90%',
		layout : 'horizontal',
		height : Ti.UI.SIZE
	});
	
	var followFacebookImage = Ti.UI.createImageView({
		top : '5dp',
		left : '15%',
		left : '20%',
		image : '/images/128Facebook.png',
		width : '16%',
		height : 'auto'
	});
	
	var followTwitterImage = Ti.UI.createImageView({
		top : '5dp',
		left : '25%',
		image : '/images/128Twitter.png',
		width : '16%',
		height : 'auto'
	});
	
	var extraHeight = Ti.UI.createView({
		height : '30%',
		width : '90%',
		backgroundColor : '#d2d3d5'
	});
	

	view.add(logo);
	view.add(titleLabel);
	view.add(titleTwoLabel);
	view.add(descriptionLabel);
	
	view.add(sponsorLabel);
	view.add(sponsorOneLabel);
	view.add(sponsorOneImage);
	
	view.add(sponsorTwoLabel);
	sponsorTwoView.add(sponsorTwoImageOne);
	sponsorTwoView.add(sponsorTwoImageTwo);
	view.add(sponsorTwoView);
	
	
	view.add(sponsorThreeLabel);
	sponsorThreeView.add(sponsorThreeImageOne);
	sponsorThreeView.add(sponsorThreeImageTwo);
	sponsorThreeView.add(sponsorThreeImageThree);
	view.add(sponsorThreeView);
	
	view.add(followLabel);
	followView.add(followFacebookImage);
	followView.add(followTwitterImage);
	view.add(followView);
	
	view.add(extraHeight);

	view.addEventListener('postlayout', function() {

		Ti.App.fireEvent('app:view_finished_loading');
	});

	return view;
};
