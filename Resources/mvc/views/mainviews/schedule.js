//
//  schedule.js
//  DrupalCamp
//
//  Created by AG Prime on 2014-05-16.
//  Copyright 2014 AG Prime. All rights reserved.
//

var uiSettings = require('/settings/uiSettings');
var strings = require('/settings/strings');

// Create the 'Main' object if it doesn't already exist
if (!Mvc.Views.Main) {

	Mvc.Views.Main = {};
}

// The login view
Mvc.Views.Main.Schedule = function(scheduleData) {

	// Hold the date
	var date = null;

	function createScheduleItem(data) {

		Ti.API.info('data' + JSON.stringify(data));		
		
		// Date title
		if (date != data.schedule_date) {

			Ti.API.info(data.schedule_date);
			// Format the date form the timestamp
			var dateFormat = moment(data.schedule_date).format('MMMM, Do');

			var dateView = Ti.UI.createView(uiSettings.scheduleDateView);
			// var dateView = Ti.UI.createView({
				// layout : 'vertical',
				// width : '90%',
				// height : '90%'
			// });

			var dateViewLabel = Ti.UI.createLabel(uiSettings.scheduleDateLabel);
			dateViewLabel.text = dateFormat;
			dateView.add(dateViewLabel);

			view.add(dateView);

			// Adding it to the date variable, so it will be added once per date
			date = data.schedule_date;
		}
		
		//
		//	The Code Above Works
		//
		
		var scheduleItem = Ti.UI.createView(uiSettings.scheduleItem);

		var title = Ti.UI.createView(uiSettings.scheduleItemTitle);

		// Giving the coffee item a 'special' color :)

		if (data.schedule_type == 'Community') {

			title.backgroundColor = uiSettings.coffeeColor;
		}
		else {

			title.backgroundColor = uiSettings.views.schedule.mainColor;
		}

		var titleLabel = Ti.UI.createLabel(uiSettings.scheduleItemTitleText);
		titleLabel.text = data.schedule_title;
		title.add(titleLabel);
		scheduleItem.add(title);

		var titleIcon = Ti.UI.createImageView({
			image : uiSettings.scheduleCategoryImages[data.schedule_type],
			width : '30dp',
			right : '7dp'
		});
		title.add(titleIcon);

		// Don't add the details if the title matches the details, it's useless

		if (data.schedule_details.toLowerCase() != data.schedule_title.toLowerCase()) {

			var scheduleDetailsWrapper = Ti.UI.createView({
				width : '94%',
				height : Ti.UI.SIZE,
				layout : 'vertical'
			});

			var scheduleDetails = Ti.UI.createLabel({
				text : data.schedule_details,
				width : '100%',
				height : Ti.UI.SIZE,
				top : 0,
				color : '#616265',
				autoLink : Ti.UI.AUTOLINK_ALL,
				font : {
					fontFamily : 'Segoe-UI-Light',
					fontSize : '16dp',
				}
			});

			scheduleDetailsWrapper.add(scheduleDetails);

			if (data.schedule_details.length > 46) {

				scheduleDetails.addEventListener('postlayout', function f() {

					scheduleDetails.top = -scheduleDetails.rect.height;
					scheduleDetails.removeEventListener('postlayout', f);
				});

				var arrow = Ti.UI.createView({
					width : '100%',
					height : '40dp',
					top : 0
				});

				var arrowImage = Ti.UI.createImageView({
					image : '/images/down.png',
					height : '30dp',
					anchorPoint : {
						x : 0.5,
						y : 0.5
					}
				});

				/*
				 * Animating the event details when the user clicks the title.
				 *
				 * Because it's a dynamic height view, i'll display it first, to get it's size
				 * and then animating it's top
				 *
				 */

				arrow.addEventListener('click', function() {

					var arrowRotateMatrix = Ti.UI.create2DMatrix({
						rotate : scheduleDetails.shown ? 0 : 180
					});

					scheduleDetails.animate({
						duration : 300,
						top : scheduleDetails.shown ? -scheduleDetails.rect.height : 0
					}, function() {

						arrowImage.animate({
							transform : arrowRotateMatrix,
							duration : 300
						});
						scheduleDetails.shown = !scheduleDetails.shown;
					});
				});

				arrow.add(arrowImage);
				scheduleDetailsWrapper.add(arrow);
			}

			scheduleItem.add(scheduleDetailsWrapper);
		}

		var dataContainer = Ti.UI.createView(uiSettings.scheduleItemData);
		scheduleItem.add(dataContainer);

		if (data.speaker_name != '-') {

			var speakerText = Ti.UI.createLabel(uiSettings.scheduleItemSpeaker);
			speakerText.text = data.speaker_name;

			if (data.company) {

				speakerText.text += ' @ ' + data.company;
			}

			dataContainer.add(speakerText);
		}

		var timeText = Ti.UI.createLabel(uiSettings.scheduleItemTime);
		timeText.text = strings.views.schedule.from + ' ' + data.schedule_time;
		dataContainer.add(timeText);

		view.add(scheduleItem);
	}

	var self = this;

	// var view = Ti.UI.createScrollView(uiSettings.viewsStyle);
	var view = Ti.UI.createScrollView({
		layout : 'vertical',
		backgroundColor : '#d2d3d5',
		// height : '300%',
		// top : '60dp'
	});

	view.update = function(data) {

		view.removeAllChildren();

		for (var model in data) {

			createScheduleItem(data[model]);
		}

		// Bottom view padding
		var bottomPadding = Ti.UI.createView({
			width : '100%',
			height : uiSettings.scheduleItem.top
		});
		view.add(bottomPadding);
	};
	if (scheduleData == null) {

		var actIndicator = new actInd();

		view.add(actIndicator);
		actIndicator.show();
	}
	else {

		view.update(scheduleData);
	}

	view.addEventListener('postlayout', function() {

		Ti.App.fireEvent('app:view_finished_loading');
	});

	return view;
};
