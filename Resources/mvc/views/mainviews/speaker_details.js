// 
//  speaker_details.js
//  DrupalCamp
//  
//  Created by AG Prime on 2014-05-16.
//  Copyright 2014 AG Prime. All rights reserved.
// 

var uiSettings = require('/settings/uiSettings');

// Create the 'Main' object if it doesn't already exist
if (!Mvc.Views.Main) {

	Mvc.Views.Main = {};
}

// The login view
Mvc.Views.Main.Speaker_Details = function() {

	var self = this;

	var view = Ti.UI.createView(uiSettings.viewsStyle);

	var label = Ti.UI.createLabel({
		text : 'speaker_details'
	});
	
	view.add(label);
	
	view.addEventListener('postlayout', function() {
		
		Ti.App.fireEvent('app:view_finished_loading');
	});
	
	return view;
};
