//
//  venue.js
//  DrupalCamp
//
//  Created by AG Prime on 2014-05-16.
//  Copyright 2014 AG Prime. All rights reserved.
//

var uiSettings = require('/settings/uiSettings');

// Create the 'Main' object if it doesn't already exist
if (!Mvc.Views.Main) {

	Mvc.Views.Main = {};
}

// The login view
Mvc.Views.Main.Venue = function() {

	var self = this;

	var view = Ti.UI.createView(uiSettings.viewsStyle);

	var MapModule = require('ti.map');

	if (android) {

		/*
		 * Android only
		 *
		 * In order to view maps, Google Play services needs to be installed on the
		 * device. Use the isGooglePlayServicesAvailable() method to check to see if
		 * Google Play services is installed.
		 */

		var rc = MapModule.isGooglePlayServicesAvailable();

		switch (rc) {
		case MapModule.SUCCESS:
			// All OK
			break;
		case MapModule.SERVICE_MISSING:
			alert('Google Play services is missing. Please install Google Play services from the Google Play store.');
			break;
		case MapModule.SERVICE_VERSION_UPDATE_REQUIRED:
			alert('Google Play services is out of date. Please update Google Play services.');
			break;
		case MapModule.SERVICE_DISABLED:
			alert('Google Play services is disabled. Please enable Google Play services.');
			break;
		case MapModule.SERVICE_INVALID:
			alert('Google Play services cannot be authenticated. Reinstall Google Play services.');
			break;
		default:
			alert('Unknown error.');
			break;
		}
	}

	var detail_view = Ti.UI.createView({
		height : '40dp',
		width : '60dp',
		backgroundColor : '#d3d4d5'
	});

	var detail_button = Ti.UI.createLabel({
		text : 'Navigate',
		color : 'white',
		font : {
			fontFamily : 'Segoe-UI',
			fontWeight : 'bold',
			fontSize : '18dp',
		}
	});

	detail_view.add(detail_button);

	var target = MapModule.createAnnotation({
		latitude : 46.773412,
		longitude : 23.620906,
		pincolor : MapModule.ANNOTATION_AZURE,
		// Even though we are creating a button, it does not respond to Button events or
		// animates.
		// Use the Map View's click event and monitor the clicksource property for
		// 'leftPane'.
		leftView : Ti.UI.createButton({
			title : 'Get Route',
			height : 32,
			width : 70
		}),
		// For eventing, use the Map View's click event
		// and monitor the clicksource property for 'rightPane'.
		title : 'Venue',
		subtitle : 'Strada Teodor Mihali 58-60'
	});

	var mapview = MapModule.createView({
		mapType : MapModule.NORMAL_TYPE,
		region : {
			latitude : 46.773412,
			longitude : 23.620906,
			latitudeDelta : 0.01,
			longitudeDelta : 0.01
		},
		annotations : [target]
	});

	mapview.addEventListener('complete', function() {

		Ti.App.fireEvent('app:view_finished_loading');
	});

	target.leftView.addEventListener('click', function(e) {

		Ti.API.info('maps e:' + JSON.stringify(e));

		// if (e.clicksource == 'leftView') {

		Ti.Platform.openURL("http://maps.google.com/maps?saddr=Current%20Location&daddr=46.773412,23.620906");
		// }

	});

	view.add(mapview);

	return view;
};
