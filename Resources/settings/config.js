// 
//  config.js
//  DrupalCamp
//  
//  Created by AG Prime on 2014-05-21.
//  Copyright 2014 AG Prime. All rights reserved.
// 

var config = {};

config.baseUrl = 'http://cluj2015.drupalcamp.ro/mobile';

config.scheduleUrl = config.baseUrl + '/views/schedule_view?display_id=schedule_mobile';
config.updateRequiredUrl = config.baseUrl + '/api/update_required';

module.exports = config;