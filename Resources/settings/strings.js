// 
//  strings.js
//  DrupalCamp
//  
//  Created by AG Prime on 2014-05-16.
//  Copyright 2014 AG Prime. All rights reserved.
// 

var strings = {
	views : {
		schedule : {
			title : 'Schedule',
			from : 'From'
		},
		speakers : {
			title : 'Speakers',
			see_all_talks : 'see all talks',
			cancel : 'cancel'
		},
		venue : {
			title : 'Venue'
		},
		info : {
			title : 'Info'
		}
	}
};


module.exports = strings;