//
//  uiSettings.js
//  DrupalCamp
//
//  Created by AG Prime on 2014-05-16.
//  Copyright 2014 AG Prime. All rights reserved.
//

var ui = {
	coffeeColor : '#a8cf45',
    views : {
        info : {
            mainColor : '#ebcc31',
            image : '/images/menu_info.png'
        },
        schedule : {
            mainColor : '#035dab',
            image : '/images/menu_program.png'
        },
        speakers : {
            mainColor : '#166c80',
            image : '/images/menu_speakers.png'
        },
        venue : {
            mainColor : '#cc2f27',
            image : '/images/menu_location.png'
        },
        breaks : {
        	mainColor : '#a8cf45'
        },
        speaker_details : {
            mainColor : '#166c80'
        },
    },
    navBar : {
        // height : '44dp',
        height : '60dp',
        width : '100%',
        top : 0,
        back : {
            backButtonOff : '/assets/buttons/backButtonBlackOff.png',
            backButtonOn : '/assets/buttons/backButtonBlackOn.png',
            left : '10dp',
            height : '28dp',
            width : '57dp'
        },
        font : {
        	fontFamily : 'Segoe-UI',
            titleColor : '#ffffff',
            titleSize : '22dp',
            titleAlign : 'center',
            buttonColor : '#ffffff',
            buttonSize : '11dp',
            buttonWeight : 'normal',
            buttonAlign : 'center',
            touchColor : '#000000'
        }
    },
    tabMenu : {
        height : '80dp',
        width : '100%',
        bottom : 0,
        options : {
            color : 'red',
            selectedColor : 'blue'
        },
        font : {
            touchColor : '#ffffff',
            textSize : '12dp',
            textWeight : 'bold',
            textAlign : 'center',
            baseColor : '#c4c4c4',
            selColor : '#f3f3f3'
        },
        button : {
            height : '100%',
            width : '100%',
            top : 0
        },
        menuItem : {
            top : 0,
            bottom : 0,
            width : '25%',
            backgroundColor : 'transparent'
        },
        selectIndicator : {
            width : '25%',
            height : '10dp',
            top : '-42dp', // hiding it, it will be displayed once the tabMenu opens from the view
        }
    },
    contentArea : {
        width : '100%',
        top : '44dp',
        bottom : '80dp'
    },
    viewsStyle : {
    	layout : 'vertical',
        backgroundColor : '#d2d3d5',
        textFont : {
            color : '#ffffff',
            size : '22dp',
            weight : 'bold',
            align : 'center'
        }
    },
    selColor : {
        backgroundColor : '#444444',
        borderColor : '#ffffff',
        font : {
            color : '#ffffff',
            size : '11dp',
            weight : 'bold',
            align : 'center'
        }
    },
    scheduleCategoryImages : {
    	'Community' : '/images/people.png',
    	'Business and Strategy' : '/images/brief.png',
    	'Sitebuilding' : '/images/cog.png',
    	'Coding and Development' : '/images/code.png',
    	'Frontend' : '/images/frontend.png'
    },
    scheduleItem : {
    	layout : 'vertical',
    	top : '14dp',
    	width : '91%',
    	height : Ti.UI.SIZE,
    	backgroundColor : '#fefefe'
    },
    scheduleItemTitle : {
    	height : '60dp',
    	width : '100%',
    	top : 0
    },
    scheduleItemTitleText : {
    	left : '10dp',
    	top : '5dp',
    	bottom : '5dp',
    	width : '85%',
    	color : 'white',
    	font : {
    		fontFamily : 'Segoe-UI-Light',
    		fontSize : '22dp',
    	}
    },
    scheduleItemData : {
    	width : '100%',
    	height : '50dp',
    	layout : 'vertical'
    },
    scheduleItemSpeaker : {
    	left : '10dp',
    	height : '23dp',
    	top : '2dp',
    	color : '#616265',
    	font : {
    		fontFamily : 'Segoe-UI-Light',
    		fontSize : '16dp',
    	}
    },
     scheduleItemTime : {
    	left : '10dp',
    	top : '2dp',
    	color : '#616265',
    	font : {
    		fontFamily : 'Segoe-UI-Light',
    		fontSize : '16dp',
    	}
    },
    scheduleDateView : {
    	top : '15dp',
    	width : Ti.UI.SIZE,
    	height : Ti.UI.SIZE
    },
    scheduleDateLabel : {
    	left : '10dp',
    	right : '10dp',
    	color : '#005DA5',
    	font : {
    		fontFamily : 'Segoe-UI',
    		fontWeight : 'bold',
    		fontSize : '18dp',
    	}
    },
    speakerItem : {
    	top : '14dp',
    	width : '91%',
    	height : Ti.UI.SIZE,
    	backgroundColor : '#fefefe', 
    	speakerItem : true
    },
    speakerImage : {
    	top : 0,
    	left : 0,
    	width : '85dp',
    	height : '127dp',
    },
    speakerName : {
    	color : '#166c80',
    	font : {
    		fontFamily : 'Segoe-UI',
    		fontSize : '20dp',
    	},
    	left : 0,
    	top : '3dp'
    },
    speakerCompany : {
    	color : '#8e8f91',
    	font : {
    		fontFamily : 'Segoe-UI-Light',
    		fontSize : '16dp',
    	},
    	left : 0,
    	top : '3dp'
    },
    speakerSeeMore : {
    	backgroundColor : '#166c80',
    	width : '123dp',
    	height : '23dp',
    	top : '99dp',
    	left : '39dp'
    }
};

module.exports = ui;
