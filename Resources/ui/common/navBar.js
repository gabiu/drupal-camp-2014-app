//
//  navBar.js
//  DrupalCamp
//
//  Created by AG Prime on 2014-05-15.
//  Copyright 2014 AG Prime. All rights reserved.
//

var uiSettings = require('/settings/uiSettings');

var loadNavBar = function(text) {

	var navBarView = Ti.UI.createView({
		backgroundColor : global_settings.CURRENTNAVCOLOR,
		height : uiSettings.navBar.height,
		width : uiSettings.navBar.width,
		top : uiSettings.navBar.top
	});

	// The title
	var navBarTitle = Ti.UI.createLabel({
		left : 10,
		right : 10,
		height : 'auto',
		textAlign : uiSettings.navBar.font.titleAlign,
		color : uiSettings.navBar.font.titleColor,
		font : {
			fontFamily : uiSettings.navBar.font.fontFamily,
			fontSize : uiSettings.navBar.font.titleSize,
			fontWeight : uiSettings.navBar.font.titleWeight
		},
		text : global_settings.CURRENTTITLE
	});
	navBarView.add(navBarTitle);
	
	

	// if (global_settings.BACKARRAY.length > 1) {
	// // The back button
	// var navBarBack = Ti.UI.createView({
	// left : uiSettings.navBar.back.left,
	// height : uiSettings.navBar.back.height,
	// width : uiSettings.navBar.back.width,
	// backgroundImage : uiSettings.navBar.back.backButtonOff
	// });
	// var navBarBackText = Ti.UI.createLabel({
	// top : 0,
	// left : 10,
	// right : 0,
	// bottom : 0,
	// text : Ti.Locale.getString('buttonBack'),
	// textAlign : uiSettings.navBar.font.buttonAlign,
	// color : uiSettings.navBar.font.buttonColor,
	// font : {
	// fontSize : uiSettings.navBar.font.buttonSize,
	// fontWeight : uiSettings.navBar.font.buttonWeight
	// }
	// });
	//
	// // Same trick as the menu to identify the area with a plain view over the top
	//
	// var navBarButtonView = Ti.UI.createView({
	// top : 0,
	// left : 0,
	// right : 0,
	// bottom : 0,
	// backgroundColor : 'transparent',
	// OPTION : global_settings.value.OPTIONS.BACK,
	// TEXT : navBarBackText,
	// IMAGE : navBarBack
	// });
	// // The back button event listener
	// navBarButtonView.addEventListener('touchstart', navBarTouch);
	// navBarButtonView.addEventListener('touchend', navBarChange);
	//
	// navBarBack.add(navBarBackText);
	// navBarView.add(navBarBack);
	// navBarView.add(navBarButtonView);
	// }

	// Update event
	Ti.App.addEventListener('app:tab_changed', function(e) {

		navBarTitle.text = global_settings.CURRENTTITLE;

		var a = Ti.UI.createAnimation({
			duration : 200,
			backgroundColor : global_settings.CURRENTNAVCOLOR
		});

		navBarView.animate(a);

		// navBarView.backgroundColor = global_settings.CURRENTNAVCOLOR;
	});
	// Always return the view object to the calling window
	return navBarView;
};

module.exports = loadNavBar;
