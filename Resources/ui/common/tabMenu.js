//
//  tabMenu.js
//  DrupalCamp
//
//  Created by AG Prime on 2014-05-15.
//  Copyright 2014 AG Prime. All rights reserved.
//

var uiSettings = require('/settings/uiSettings');
var strings = require('/settings/strings');

/*
 * createMenuItem
 * ==============
 *
 * This function create each menu item with the parameters sent in.
 */

function createMenuItem(inParam) {

	var menuItem = Ti.UI.createView(uiSettings.tabMenu.menuItem);
	menuItem.left = inParam.left;

	menuItem.VIEW = inParam.view;

	// Put the icon on the button
	var menuButton = Ti.UI.createView({
		top : uiSettings.tabMenu.button.top,
		height : uiSettings.tabMenu.button.height,
		width : uiSettings.tabMenu.button.width,
		backgroundColor : inParam.color,
	});

	var menuIcon = Ti.UI.createImageView({
		height : '60%',
		image : inParam.image
	});

	// Put a view over the top of the main container so it will always be the source
	// for the event listener

	var buttonView = Ti.UI.createView({
		top : 0,
		left : 0,
		right : 0,
		bottom : 0,
		backgroundColor : 'transparent',
		VIEW : inParam.view,
	});
	
	// Add the event listener to action the controller and set the selected
	buttonView.addEventListener('touchend', function(e) {
		
		if (global_settings.CURRENTOPTION != e.source.VIEW && menuItem.parent.viewLoaded !== false && menuItem.parent.indicatorAnimationFinished !== false) {
			
			/*
			 * Preventing more touches until this view loads, to avoid UI overload and possible crashes
			 */
			menuItem.parent.viewLoaded = false;
			menuItem.parent.indicatorAnimationFinished = false;
			
			Mvc.Views['Main'].action(e.source.VIEW);
		}
	});

	menuButton.add(menuIcon);
	menuItem.add(menuButton);
	menuItem.add(buttonView);

	return menuItem;
}

function loadTabMenu(inParam) {

	// An array to hold the buttons to be iterated by the indicator
	var tabButtons = [];

	// Including the animate module
	var Animator = require('com.animecyc.animator');

	var tabMenuView = Ti.UI.createView({
		backgroundColor : 'white',
		height : uiSettings.tabMenu.height,
		width : uiSettings.tabMenu.width,
		bottom : uiSettings.tabMenu.bottom,
	});

	// Build each menu option

	// In this example we build 4, each taking 25% of the space, as specified in
	// uiSettings. You can see how to modify this down if needed.
	// Really do not go over 5 as that is the ideal size for phone menus.
	//
	// The left param is hardcoded. Keep this in mind if you want to modify anything

	// Create the menu options
	var schedule = createMenuItem({
		view : 'Main.Schedule',
		image : uiSettings.views.schedule.image,
		color : uiSettings.views.schedule.mainColor,
		left : 0
	});

	tabButtons.push(schedule);

	var speakers = createMenuItem({
		view : 'Main.Speakers',
		image : uiSettings.views.speakers.image,
		color : uiSettings.views.speakers.mainColor,
		left : '25%'
	});

	tabButtons.push(speakers);

	var venue = createMenuItem({
		view : 'Main.Venue',
		image : uiSettings.views.venue.image,
		color : uiSettings.views.venue.mainColor,
		left : '50%'
	});

	tabButtons.push(venue);

	var info = createMenuItem({
		view : 'Main.Info',
		image : uiSettings.views.info.image,
		color : uiSettings.views.info.mainColor,
		left : '75%'
	});

	tabButtons.push(info);

	// Add the menu options
	tabMenuView.add(schedule);
	tabMenuView.add(speakers);
	tabMenuView.add(venue);
	tabMenuView.add(info);

	/*
	 * Current selection indicator
	 *
	 * We know that the first option is the Schedule item, so we'll display the
	 * indicator there when the app starts
	 */

	var indicator = Ti.UI.createView(uiSettings.tabMenu.selectIndicator);
	var indicatorImage = Ti.UI.createImageView({
		image : '/images/caret.png',
		height : uiSettings.tabMenu.selectIndicator.height,
	});
	
	indicator.add(indicatorImage);

	indicator.left = 0;
	indicator.top = 0;
	
	Ti.App.addEventListener('app:view_finished_loading', function() {
		
		tabMenuView.viewLoaded = true;
	});
	
	Ti.App.addEventListener('app:tab_changed', function(e) {

		for (var button in tabButtons) {

			if (tabButtons[button].VIEW == global_settings.CURRENTOPTION) {

				Animator.animate(indicator, {
					duration : 500,
					easing : Animator.BACK_IN_OUT,
					left : tabButtons[button].left
				}, function() {
					
					tabMenuView.indicatorAnimationFinished = true;
				});
			}
		}
	});

	tabMenuView.add(indicator);

	return tabMenuView;
}

module.exports = loadTabMenu;
